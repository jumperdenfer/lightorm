<?php
namespace LightormExemple;
require_once('..\vendor\autoload.php');

use Lightorm\Model;
use LightormExemple\PropositionModel;
//Phase de test
$host = 'mysql:host=localhost;dbname=cour_php';
$userLogin = "root";
$userPass = '';
class User extends Model{

    public function __construct(){
        $this->table = "utilisateur";
        $this->dbStart();
    }

}
//Test select specific user in my  local DB
$userSelect = (new User())->select(['id','password',"pseudo"])->where("pseudo","=","jumperAlpha")->first();
echo "<pre>";
var_dump($userSelect);
echo "</pre>";

//Test add user in my local DB
$userAdd = (new User())
            ->insert()
            ->data('pseudo',"jumperModel")
            ->data('age',24)
            ->data('password','testpass')
            ->data('mail',"test@test.fr")
            ->exect();
echo "<pre>";
var_dump($userAdd);
echo "</pre>";

//Test delete user
$userDelete = (new User())
->delete()
->where('pseudo',"=",'jumperModel')
->exect();
var_dump($userDelete);

//Test select With
$userWithProposition = (new User())
->select()
->where('id','=','1')
->first()
->with(new PropositionModel);
echo "<pre>";
var_dump($userWithProposition);
echo "</pre>";
