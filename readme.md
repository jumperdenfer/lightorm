# Light ORM
## benoitlagrange38@hotmail.fr

# Objectif:
Permettre à des utilisateurs de PHP natif d'avoir accès à un pseudo ORM facilitant la création de classe d'accès à des bases de données.

# Fonctionnement:

- Commencez par créer une classe en suivant les normes de votre projet
    - (Exemple : maClass.Model.php)
- Implement la classe ModelGestion en héritage
    - (Exemple : class MaClass extends ModelGestion)

- Une fois cela fait, vous obtenez l'accès aux méthode suivantes:

- insert()
    - Permets d'insérer des données
- select()
    - Permets de sélectionner des données
- update()
    - Permets de mettre à jour des données
- delete()
    - Permets de supprimer des données

Via ces méthodes, vous pouvez accéder aux CRUD classique en plus de ces méthodes, des méthodes complémentaires sont existantes:

- data('nomColonne',$params)
    - Permets de sélectionner des colonnes, dans le cas d'une update, définit les colonnes à changer, il peut y avoir plusieurs modifications en chainant la méthode data
- where('nomColonne',$params)
    - Permets de définir une clause dans votre requête, il peut y avoir plusieurs clauses en chainant la méthode where.
- with()
    - Permets de récupérer des données d'une sous-requête définie dans le modèle.
- whereHas()
    - Permets d'ajouter une clause dans une sous-requête définie dans le modèle.
- limiteBy()
    - Définit le nombre de résultat à afficher
- sort('nomColonne','ASC/DESC')
    - Définis un order by pour la colonne sélectionnée en ascendant et descendant en fonction du second paramètre.

Enfin les méthodes d'accès aux donnée qui permette d'executer la requête

- get()
    - Récupère le jeu de données complet.
- first()
    - Récupère uniquement la première entrée, la method " limite()" est ignorée dans ces conditions.
- test()
    - Permets de tester la requête afin de voir si elle réussit ou non ( ne s'applique pas pour le select()).

# Pourquoi utiliser cette librairie ?

Avant toute chose, cette librairie ne remplace pas les principaux systèmes ORM de PHP ( comme Eloquent de laravel par exemple). Cette librairie est créée dans le but de permettre au développeur débutant ou travaillant principalement sur du natif ( sans framework), d'avoir accès à un call simplifié des requêtes PDO.

Il est fortement conseillé de connaitre le fonctionnement de PDO avant d'utiliser cette librairie.

# Conclusion:
- Cette librairie n'est pas faite pour gérait des gros projets ou êtres utilisés avec des frameworks comme Symfony ou laravel ( qui dispose de leur propre ORM Doctrine / Eloquent).
- La connaissance de PDO est nécessaire.
- Permets aux développeurs natifs d'avoir accès à des requêtes simplifier
- Permets aux développeurs natifs d'intégrer le principe de Model dans leur projet ( voir MVC).
- Permets de limiter les erreurs de requête PDO
