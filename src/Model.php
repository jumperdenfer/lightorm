<?php
namespace Lightorm;
use \PDO;
class Model {
    public $table = ''; //Need to overWrite in extendeds Class.
    private $request; //Type of request
    private $where; //Clause
    private $data; //columns for placeholder binding in request ' exemple = :exemple'
    private $listParams; // params for binding
    private $limit; //Limit for get method only;
    private $orderBy; //order by for Select request only;
    private $db; //access to databse, can ba a singleton, here we use dbStart with global var.
    /**
    * dbStart
    *
    * Connection to a specified database with global variable
    * can be deleted if you use an another system of connection, like a singleton.
    */
    public function dbStart(){
        //Se connecte à la base de données correspondante
        global $host;
        global $userLogin;
        global $userPass;
        $this->db = new PDO($host,$userLogin,$userPass);
    }
    //Starting method
    /**
    * Insert
    *
    * Specify the type of SQL Request to Insert
    * Add the request in the private variable Request
    * @return this
    */
    public function insert(){
        $this->request = "INSERT INTO {$this->table}";
        return $this;
    }
    /**
    * Select
    *
    * Specify the type of SQL request to select
    *
    * @param Array optionnal, list of wented column from your table
    * @return this
    */
    public function select(array $arrayColumn = []){
        if(!empty($arrayColumn)){
            foreach ($arrayColumn as $column) {
                if(!isset($columnRequest)){
                    $columnRequest = "{$column}";
                }
                else{
                    $columnRequest .= ",{$column}";
                }
            }
            $this->request = "SELECT {$columnRequest} FROM {$this->table}";
            return $this;
        }
        else{
            $this->request = "SELECT * FROM {$this->table}";
            return $this;
        }

    }
    /**
    * Update
    * Specify the type of SQL request to Update
    *
    *@return this
    */
    public function update(){
        $this->request = "UPDATE {$this->table}";
        return $this;
    }
    /**
    * Delete
    *
    * Specify the type of SQL request to Delete
    * @return this
    */
    public function delete(){
        $this->request = "DELETE FROM {$this->table}";
        return $this;
    }
    //SubQuery Method
    /**
    * Data
    *
    * Select column for insert / update and add the binding params in list params
    * @param Column String, name of the column to insert/update
    * @param Params String, data bind to the column
    * @return this
    */
    public function data(string $column, string $params){
        if($this->data == NULL){
            $this->data = "SET {$column} = :{$column}";
        }
        else{
            $this->data.= ",{$column} = :{$column}";
        }
        $this->listParams[] = ['column' => $column, 'bind' => $params];
        return $this;

    }
    /**
    * Where
    *
    * Add a where Clause to the request
    * @param Column, Name of the column for where clause
    * @param Operator, operator of the where clause (ex: '=' , '>')
    * @param Params, data binded to the where clause
    * @return this
    */
    public function where(string $column, string $operator,string $params){
        if($this->where == null){
            $this->where = "WHERE {$column} {$operator} :{$column}";
        }
        else{
            $this->where.= "AND {$column} {$operator} :{$column}";
        }
        $this->listParams[] = ['column' => $column, 'bind' => $params];
        return $this;
    }
    /**
    * With
    * Add a sub-request to the current request
    *
    * Add this after execute the request /!\
    * @param Model, CLass of the wanted Table (don't forget to extends Model on the selected class)
    * @param ArrayParams List of 'column' and 'data' for specific select in the sub-request
    * @return this
    */
    public function with(Model $model,array $arrayParams = []){
        $idType = "id_{$this->table}";
        $dataModel = $model->select($arrayParams)->where("{$idType}","=","{$this->id}")->get();
        $this->with = $dataModel;
        return $this;
    }
    /**
    * WhereHas
    *
    * Add a where clause with for a subrequest validation
    * @param Table Name of the table for subRequest 
    * @param ForeignKey Name of the foreign key (ex: id);
    * @param whereParams Name of the column for where (ex: id_user);
    */
    public function whereHas($table,string $foreignKey,string $whereParams){
        throw new \Exception('Not implemented yet!');
        if($this->where == null){
            $this->where = "WHERE {$params} IN (SELECT {$foreignKey} FROM {$table})";
        }
        else{
            $this->where = "AND ${params} IN (SELECT {$foreignKey} FROM {$table})";
        }
        return $this;
    }
    /**
    * limitBy
    *
    * Add a limit of data for the request (only on GET type)
    * @param limit, int , number of limit
    * @return this
    */
    public function limitBy($limit){
        $this->limit = $limit;
        return $this;
    }
    /**
    * Sort
    *
    * Add an Order by on the wanted column
    * @param Array, List of column for Order by
    * @param Sort Type of Order By ( ASC or DESC)
    * @return this
    */
    public function sort(Array $columns,String $sort){
        $sort = strtoupper($sort);
        foreach ($columns as $column) {
            if($this->orderBy == null){
                $this->orderBy .= "ORDER BY {$column} {$sort}";
            }
            else{
                $this->orderBy .= ", {$column} {$sort}";
            }
        }
        return $this;
    }
    //Method for execute
    /**
    * Get
    *
    * Execute the query with all params. Use thios only on Select Request
    * @return this With multiple enrty / data
    */
    public function get(){
        $requestExecute = ($this->db)->prepare("{$this->request} {$this->where} {$this->orderBy} {$this->limit}");
        $requestExecute->setFetchMode(PDO::FETCH_INTO, $this);
        if(!empty($this->listParams)){
            foreach($this->listParams as $keys => $value){
                $column = $value['column'];
                $bindedData = $value['bind'];
                $requestExecute->bindValue(":{$column}",$bindedData);
            }
        }
        $testingRequest = $requestExecute->execute();
        $dataObj = $requestExecute->fetchAll();
        $error = $requestExecute->errorInfo();
        if($testingRequest == true){
            return $dataObj;
        }
        else{
            return $error;
        }
    }
    /**
    * First
    * Same as Get but with only one data; Use this only on Select request
    *
    * @return this with one entry /data
    */
    public function first(){
        $requestExecute = ($this->db)->prepare("{$this->request} {$this->where} LIMIT 1");
        if(!empty($this->listParams)){
            foreach($this->listParams as $keys => $value){
                $column = $value['column'];
                $bindedData = $value['bind'];
                $requestExecute->bindValue(":{$column}",$bindedData);
            }
        }
        $requestExecute->setFetchMode(PDO::FETCH_INTO, $this);
        $testingRequest = $requestExecute->execute();
        $dataObj = $requestExecute->fetch();
        $error = $requestExecute->errorInfo();
        if($testingRequest == true){
            return $this;
        }
        else{
            return $error;
        }
    }
    /**
    * exect
    *
    * Specific request for Insert , Update and Delete. Create a transaction with rollback on error.
    * @return Boolean
    */
    public function exect(){
        ($this->db)->beginTransaction();
        $requestExecute = ($this->db)->prepare("{$this->request} {$this->data} {$this->where}");
        if(!empty($this->listParams)){
            foreach($this->listParams as $keys => $value){
                $column = $value['column'];
                $bindedData = $value['bind'];
                $requestExecute->bindValue(":{$column}","{$bindedData}");
            }
        }
        $testingRequest = $requestExecute->execute();
        $error = $requestExecute->errorInfo();
        if($testingRequest == true){
            $this->db->commit();
            return $testingRequest;
        }
        else{
            $this->db->rollback();
            throw new \Exception('An error was occured, request was rollback');
        }
    }
}
